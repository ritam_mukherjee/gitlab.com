package interface_property;

public interface Direct_Execution {
    default void default_method(){
        System.out.println("Interface default method execution");
    }
    static void static_method(){
        System.out.println("interface static method execution");
    }
    public static void main(String[] args) {
        System.out.println("Interface main() method execution");
        static_method();
    }
}
