package streams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Stream;


public class StreamInAction {

    public static void main(String[] args) {

        // Stream<String> name_stream=Stream.of("one","two","three","four","five","six","seven","eight","nine","zero");

        String[] number_array = {"one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "zero"};

        System.out.print(Colors.ANSI_PURPLE + "===================STREAM OPERATIONS=====================");

        ArrayList<String> number_list = new ArrayList<String>(Arrays.asList(number_array));
        System.out.println(Colors.ANSI_RED + "\n-Stream Operation over Collection::" + Colors.ANSI_RESET);
        number_list.stream().forEach(s -> System.out.print(s + ","));

        System.out.println(Colors.ANSI_RED + "\n-Stream Operation over Array::" + Colors.ANSI_RESET);
        Arrays.stream(number_array).forEach(s -> System.out.print(s + ","));

        System.out.println(Colors.ANSI_BLUE + "\n-Filter Operation Over Stream ArrayList::" + Colors.ANSI_RESET);
        number_list.stream().filter(s -> s.length() > 3).forEach(s -> System.out.print(s + ","));

        System.out.println(Colors.ANSI_BLUE + "\n-Filter Operation Over Stream Array::" + Colors.ANSI_RESET);
        Arrays.stream(number_array).filter(s -> s.length() > 3).forEach(s -> System.out.print(s + ","));

        System.out.println(Colors.ANSI_BLUE + "\n-Attaching Multiple Filter Over ArrayList::" + Colors.ANSI_RESET);
        number_list.stream()
                .filter(s -> s.length() > 2)
                .filter(s -> s.length() < 5)
                .forEach(s -> System.out.print(s + ","));


        System.out.println(Colors.ANSI_BLUE + "\n-Attaching Multiple Filter Over Stream Array::" + Colors.ANSI_RESET);
        Arrays.stream(number_array)
                .filter(s -> s.length() > 2)
                .filter(s -> s.length() < 5)
                .forEach(s -> System.out.print(s + ","));

        System.out.println(Colors.ANSI_GREEN + "\n-Direct Stream declaration using OF method" + Colors.ANSI_RESET);
        Stream<String> days_stream = Stream
                .of("SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY");

        days_stream.forEach(s -> System.out.print(s.toLowerCase().substring(0, s.length() - 3) + ","));

        System.out.println(Colors.ANSI_GREEN + "\n-Adding Array element to List" + Colors.ANSI_RESET);
        ArrayList<String> list = new ArrayList<>();
        Arrays.stream(number_array)
                .peek(s -> System.out.print(s + ","))
                .filter(s -> s.length() > 3)
                .forEach(list::add);
        System.out.println("SIZE IS:" + list.size());


    }
}
