package predefined_functions.predicates;

import predefined_functions.Colors;

import java.util.Arrays;
import java.util.Collection;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class PredicateProgram1 {
    public static void main(String[] args) {

        Predicate<Integer> bigger_than_fifty = integer -> integer > 50;
        Predicate<Integer> smaller_than_hundred = integer -> integer < 100;

        Consumer<String> printConsumer = System.out::println;

        System.out.println(Colors.ANSI_BLUE + "Testing test() method of predicate-----");
        System.out.println(bigger_than_fifty.test(55));
        System.out.println(smaller_than_hundred.test(55));

        Predicate<Collection<String>> is_collection_empty = strings -> strings.isEmpty();

        System.out.println(is_collection_empty.test(Arrays.asList("Tom", "Dik", "Harry")));


        System.out.println(Colors.ANSI_RED + "Testing negate() operator where complement of the condition validated:");
        System.out.println(bigger_than_fifty.negate().test(48));
        System.out.println(smaller_than_hundred.negate().test(56));

        System.out.println(Colors.ANSI_CYAN + "Testing or operator where all conditions are validated:");
        System.out.println(bigger_than_fifty.and(smaller_than_hundred).test(55));
        System.out.println(bigger_than_fifty.and(smaller_than_hundred).test(155));

        System.out.println(Colors.ANSI_GREEN + "Testing or operator where at least  one condition should valid:");
        System.out.println(bigger_than_fifty.or(smaller_than_hundred).test(55));
        System.out.println(bigger_than_fifty.or(smaller_than_hundred).test(155));

    }
}
