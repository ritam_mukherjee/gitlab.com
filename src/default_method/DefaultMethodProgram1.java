package default_method;
interface left {
    default void method(){
        System.out.println("left interface: default method()");
    }
}
interface right{
    default void method(){
        System.out.println("right interface: default method()");
    }
}

public class DefaultMethodProgram1 implements left,right{
    @Override
    public void method() {
        /*If we want new Implementation*/
        System.out.println("Child class: default method");
        /*If we want left class Implementation*/
        left.super.method();
        /*If we want right class Implementation*/
        right.super.method();
    }

    public static void main(String[] args) {
        new DefaultMethodProgram1().method();
    }
}
